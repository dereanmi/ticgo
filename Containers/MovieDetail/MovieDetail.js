import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, ActivityIndicator, ListItem, Alert, FlatList } from 'react-native';
import { Icon, SearchBar, TabBar, Tabs, List, Flex, Grid, ListView, Button, Carousel } from '@ant-design/react-native';
import axios from 'axios'
var options = { weekday: 'short', month: 'short', day: 'numeric' };

var tab1 = new Date();
date1 = tab1.getTime()
day1 = tab1.toLocaleDateString("en-US", options)
var tab2 = new Date(new Date().getTime() + 24 * 60 * 60 * 1000)
date2 = tab2.getTime()
day2 = tab2.toLocaleDateString("en-US", options)
var tab3 = new Date(new Date().getTime() + 48 * 60 * 60 * 1000)
date3 = tab3.getTime()
day3 = tab3.toLocaleDateString("en-US", options)


class MovieDetail extends Component {


    onClickBack = () => {
        this.props.history.push('./home')

    }

    // onClickProfile = () => {
    //     this.props.history.push('/profile')
    // }
    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'Showing',
            items: [],
            isLoading: true,
            showtime1: [],
            showtime2: [],
            showtime3: [],
            isLoadingShowing: true,
            tabs: [
                { title: day1 },
                { title: day2 },
                { title: day3 },
            ],
            dates: [
                { title: date1 },
                { title: date2 },
                { title: date3 },
            ]
            // tabs: [
            //     { title: day1, date: tab1 },
            //     { title: day2, date: tab2 },
            //     { title: day3, date: tab3 },
            // ]

        };
    }

    onChangeTab() {
        this.setState({
            selectedTab: tabName,
        });
    }


    getMovie = () => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes${this.props.location.state.item._id}`)
            .then(response => {
                this.setState({
                    items: response.data,
                    isLoadingShowing: false
                })
            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    getMovieByDate1 = () => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.item._id}`, {
            params: {
                date: date1
            }
        })
            .then(response => {
                this.setState({
                    showtime1: response.data,
                    isLoading: false
                })
            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    getMovieByDate2 = () => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.item._id}`, {
            params: {
                date: date2
            }
        })
            .then(response => {
                this.setState({
                    showtime2: response.data,
                    isLoading: false
                })
            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    getMovieByDate3 = () => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.item._id}`, {
            params: {
                date: date3
            }
        })
            .then(response => {
                this.setState({
                    showtime3: response.data,
                    isLoading: false
                })
            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    UNSAFE_componentWillMount() {
        console.log(this.props)
        if (this.props.location && this.props.location.state && this.props.location.state.idmovie) {
            Alert.alert('Your number is', this.props.location.state.idmovie + '')
        }

    }

    clickTime = (item) => {
        console.log(item);
        
        this.props.history.push('./cinema', { item: item ,name:this.props.location.state.item})

    }


    componentDidMount() {
        this.getMovie()
        this.getMovieByDate1()
        this.getMovieByDate2()
        this.getMovieByDate3()
    }

    render() {
        const movie = this.props.location.state.item
        // console.log(this.state.movies1);
        // console.log(this.state.movies2);
        // console.log(this.state.movies3);


        return (


            <TabBar
                unselectedTintColor="white"
                tintColor="#AC3333"
                barTintColor="#AC3333"
            >
                <TabBar.Item
                    title="MOVIES"
                    icon={<Icon name="video-camera" />}
                // selected={this.state.selectedTab === 'Showing'}
                // onPress={() => this.onChangeTab('Showing')}
                >
                    <View style={[styles.contents]}>

                        <View style={styles.header}>

                            <TouchableOpacity
                                onPress={this.onClickBack}
                            >
                                <View style={[styles.rowIcon]}>
                                    <Icon name="left" size="md" color="white" />
                                </View>
                            </TouchableOpacity>

                            <View style={[styles.rowHeader]}>
                                <Text style={[styles.textname]}>{movie.name}</Text>
                            </View>

                        </View>
                        <ScrollView>
                            <View style={[styles.boxContent]}>

                                <View style={[styles.box1]}>

                                    <View style={[styles.rowHeader]}>
                                        <Image source={{ uri: movie.image }}
                                            style={{ width: '100%', height: 250 }} />
                                    </View>


                                    <View style={[styles.rowHeader, styles.center]}>
                                        <Text style={styles.movieText}>
                                            {movie.name}
                                        </Text>
                                        <Text style={styles.movieText}>
                                            <Icon name="clock-circle" size="md" color="#AC3333" /> {movie.duration} min
                                        </Text>
                                    </View>
                                </View>
                                <View style={[styles.box1]}>
                                    <Tabs
                                        tabs={this.state.tabs}
                                        tabBarPosition='top'


                                    >
                                        <View>
                                            <Grid
                                                data={this.state.showtime1}
                                                hasLine={false}
                                                renderItem={(item) => (
                                                    <View style={{ flex: 1, margin: 3 }}>
                                                        <Text style={{ color: "black", fontSize: 12 }}>
                                                            <Icon name="sound" size="xxs" color="#AC3333" />{" "}
                                                            {item.soundtrack}
                                                        </Text>
                                                        <Button onPress={()=>this.clickTime(item)}>
                                                            <Text style={{ fontSize: 10 }}>
                                                                {new Date(item.startDateTime).toLocaleTimeString()}
                                                            </Text>
                                                        </Button >
                                                    </View>
                                                )}
                                            />
                                        </View>
                                        <View>
                                            <Grid
                                                data={this.state.showtime2}
                                                hasLine={false}
                                                renderItem={(item) => (
                                                    <View style={{ flex: 1, margin: 3 }}>
                                                        <Text style={{ color: "black", fontSize: 12 }}>
                                                            <Icon name="sound" size="xxs" color="#AC3333" />{" "}
                                                            {item.soundtrack}
                                                        </Text>
                                                        <Button onPress={()=>this.clickTime(item)}>
                                                            <Text style={{ fontSize: 10 }}>
                                                                {new Date(item.startDateTime).toLocaleTimeString()}
                                                            </Text>
                                                        </Button >
                                                    </View>
                                                )}
                                            />
                                        </View>
                                        <View>
                                            <Grid
                                                data={this.state.showtime3}
                                                hasLine={false}
                                                renderItem={(item) => (
                                                    <View style={{ flex: 1, margin: 3 }}>
                                                        <Text style={{ color: "black", fontSize: 12 }}>
                                                            <Icon name="sound" size="xxs" color="#AC3333" />{" "}
                                                            {item.soundtrack}
                                                        </Text>
                                                        <Button onPress={()=>this.clickTime(item)}>
                                                            <Text style={{ fontSize: 10 }}>
                                                                {new Date(item.startDateTime).toLocaleTimeString()}
                                                            </Text>
                                                        </Button >
                                                    </View>
                                                )}
                                            
                                            />
                                        </View>

                                    </Tabs>

                                </View>
                            </View>

                        </ScrollView>
                    </View>
                </TabBar.Item>


                <TabBar.Item
                    icon={<Icon name="heart" />}
                    title="MY MOVIES"
                //   selected={this.state.selectedTab === 'Faverite'}
                //   onPress={this.onClickFaverite}
                />

                <TabBar.Item
                    icon={<Icon name="reload-time" />}
                    title="BOOKING"
                //   selected={this.state.selectedTab === 'Booking'}
                //   onPress={() => this.onChangeTab('Booking')}
                />


                <TabBar.Item
                    icon={<Icon name="user" />}
                    title="PROFILE"
                    selected={this.state.selectedTab === 'profile'}
                    onPress={this.goToLProfile}
                />
            </TabBar>

        );
    }
}

export default MovieDetail

const styles = StyleSheet.create({
    content: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },

    imagesize: {
        width: 50,
        height: 50,
        marginLeft: 10

    },
    center: {
        alignItems: 'center',
        //justifyContent: 'center'
    },
    button: {
        backgroundColor: '#AC3333',
        borderColor: '#AC3333'
    },
    textname: {
        textAlign: 'center',
        fontSize: 20,
        color: 'white',
    },
    movieText: {
        textAlign: 'center',
        fontSize: 20,
        color: 'black',

    },
    header: {
        flexDirection: 'row',
        backgroundColor: '#AC3333',
        borderBottomWidth: 3,
        borderBottomColor: 'white',
        justifyContent: 'center',
        padding: 15,

    },
    boxContent: {
        flexDirection: 'column',
        backgroundColor: 'white',
        justifyContent: 'center',
        margin: 10

    },
    rowHeader: {
        flex: 1,
    },

    rowIcon: {
        flex: 0,
    },

    box1: {
        flex: 1,
        flexDirection: 'row',
        height: 250
    },

    box2: {
        flex: 1
    }
});
