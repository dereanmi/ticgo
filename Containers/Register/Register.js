import React, { Component } from 'react';
import { Text, View, ImageBackground, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { Button, InputItem, WhiteSpace } from '@ant-design/react-native';
import { Link } from 'react-router-native'
import axios from 'axios';

class Register extends React.Component {

    
    state = {
        username: '',
        password: '',
        email: '',
        firstname: '',
        lastname: ''
    };

    goToLogin = () => {
        this.props.history.push('/login', {
        })
    }

    goToHome = () => {
        this.props.history.push('/home', { user: this.state.email, password: this.state.password })
    }

    goToRegister = () => {
        axios({
            url: 'https://zenon.onthewifi.com/ticGo/users/register?email',
            method: 'post',
            data: {
                email: this.state.email,
                password: this.state.password,
                firstName: this.state.firstName,
                lastName: this.state.lastName,
              
            }
        }).then(res => {
            const { data } = res
            const { user } = data
            this.props.history.push('/login')
        }).catch(e => {
            console.log("error ", e.response)
        })
    }


    render() {
        return (
            <ImageBackground source={require('./register-bg.jpg')} style={styles.Background}>
                <View style={[styles.container, styles.transparent]} >
                    <View style={styles.header}>
                        <Text style={styles.textHeader}> Sign up now</Text>
                        <View style={styles.center}>
                            <Image source={require('./profile-icon.png')} style={styles.profile} />
                        </View>
                    </View>
                    <View style={[styles.content]}>
                        <View style={[styles.layout1, styles.centerLayout1]}>
                            <Text style={styles.textStyle}>
                            </Text>
                            <InputItem
                                type="email"
                                value={this.state.email}
                                onChange={email => { this.setState({ email, }); }}
                                placeholder="E-mail"
                            >
                                <Image source={require('./username-icon.png')} style={styles.usernameIcon} />
                            </InputItem>
                            <WhiteSpace></WhiteSpace>
                            <InputItem
                                type="text"
                                clear
                                value={this.state.firstName}
                                onChange={firstName=> { this.setState({ firstName , }); }}
                                placeholder="First Name"

                            >
                                <Image source={require('./username-icon.png')} style={styles.usernameIcon} />
                            </InputItem>
                            <WhiteSpace></WhiteSpace>
                            <InputItem
                                 type="text"
                                 value={this.state.lastName}
                                 onChange={lastName => { this.setState({ lastName, }); }}
                                 placeholder="Last Name"
 
                            >
                                <Image source={require('./username-icon.png')} style={styles.usernameIcon} />
                            </InputItem>
                            <WhiteSpace></WhiteSpace>
                            <InputItem
                                 type="number"
                                 value={this.state.password}
                                 onChange={password=> { this.setState({ password, }); }}
                                 placeholder="Password"
 
                            >
                                <Image source={require('./password-icon.png')} style={styles.passwordIcon} />
                            </InputItem>


                        </View>
                        <View style={[styles.layout2, styles.centerLayout2]}>




                            <View style={styles.loginmiddle}>
                                <View style={styles.loginButton}>
                                    <TouchableOpacity onPress={this.goToRegister}>
                                        <Text style={styles.textButton}>Save</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.loginFooter}>
                                <View>
                                    <TouchableOpacity onPress={this.goToLogin}>
                                        <Text style={{ fontSize: 17, color: 'black' }}>Back to login page</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                        </View>
                    </View>
                </View >

            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({

    Background: {
        width: '100%',
        height: '100%'
    },

    container: {
        flex: 1
    },

    header: {
        backgroundColor: '#AC3333',
        alignItems: 'center',
        flex: 0.1,
        flexDirection: 'row'
    },

    box1: {
        flex: 1,
        margin: 14,
        alignItems: 'center',
        justifyContent: 'center'
    },

    box2: {
        flex: 1,
        margin: 14,
        alignItems: 'center',
        justifyContent: 'center'
    },

    content: {
        flex: 1,
        flexDirection: 'column'

    },
    layout1: {

        flex: 1,
        flexDirection: 'column'
    },

    layout2: {

        flex: 0.5,
        flexDirection: 'column'
    },

    loginmiddle: {
        flex: 0.5,
        flexDirection: 'row'
    },

    loginFooter: {
        flex: 0.5,
        flexDirection: 'row'
    },

    profile: {
        borderRadius: 25,
        width: 50,
        height: 50
    },

    usernameIcon: {
        borderRadius: 10,
        width: 20,
        height: 20
    },

    passwordIcon: {
        borderRadius: 10,
        width: 30,
        height: 30
    },

    loginButton: {
        backgroundColor: '#AC3333',
        flex: 1,
        margin: 20,
        borderRadius: 5,
        width: 100,
        height: 40
    },

    textHeader: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 15
    },

    textStyle: {
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
        padding: 15
    },

    textButton: {
        color: 'black',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20
    },

    centerLayout1: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    centerLayout2: {
        alignItems: 'center',
        // justifyContent: 'center'
    },

    transparent: {
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
        flex: 1,
        margin: 15
    }

})
export default Register
