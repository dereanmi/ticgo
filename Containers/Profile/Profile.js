import React, { Component } from 'react';
import { Text, View, ImageBackground, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Button, InputItem, WhiteSpace } from '@ant-design/react-native';
import ImagePicker from 'react-native-image-picker'
import axios from 'axios'
import { connect } from 'react-redux'

class Profile extends React.Component {

    state = {
        username: '',
        email: '',
        password: '',
        firstname: '',
        lastname: '',
        imagePath: ''
    };

    goToLogin = () => {
        this.props.history.push('/login', {
        })
    }

    goToHome = () => {
        this.props.history.push('/home', {
        })
    }

    gotoEditProfile = () => {
        this.props.history.push('/editprofile', {
            
        })
    }

    gotoHistory = () => {
        const { push } = this.props
        push('/history')
    }

    selectImage = () => {
        console.log("55", ImagePicker)
        console.log("pros token", this.props.user[0].user.token)
        // console.log("55",ImagePicker.showImagePicker)
        // console.log("55",ImagePicker.showImagePicker())
        ImagePicker.showImagePicker({}, (response) => {
            console.log(response)
            if (response.uri) {
                const formData = new FormData()
                formData.append('image', {
                    uri: response.uri,
                    name: response.fileName,
                    type: response.type,
                })
                axios.post('https://zenon.onthewifi.com/ticGo/users/image', formData, {
                    headers: {
                        Authorization: `Bearer ${this.props.user[0].user.token}`
                    },
                    onUploadProgress: progressEvent => {
                        console.log('progress', Math.floor(progressEvent.loaded / progressEvent.total * 100))
                    }
                })
                    .then(response => {
                        this.setState({
                            imagePath: response.data.image
                        })
                    })
                    .catch(error => {
                        console.log("errror rrrr", error.response)
                    })
            }
        })
    }

    componentDidMount() {
        this.getImage()
    }

    getImage = () => {
        const { user } = this.props
        axios.get('https://zenon.onthewifi.com/ticGo/users', {
            headers: {
                Authorization: `Bearer ${user[0].user.token}`
            }
        })
            .then(response => {
                this.setState({
                    email:response.data.user.email,
                    firstname:response.data.user.firstName,
                    lastname:response.data.user.lastName,
                    imagePath: response.data.user.image,
                    isLoading: false
                })


            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })
    }

    render() {
        const { user } = this.props
        console.log(user)
        console.log(this.state.firstname);
        console.log(this.state.lastname);


        return (
            <ScrollView contentContainerStyle={styles.contentContainer}>
                <ImageBackground source={require('./profile-bg.jpg')} style={styles.Background}>
                    <View style={[styles.container1, styles.transparent, styles.border]} >
                        <View style={styles.header}>
                            <View style={styles.headerBox1}>
                                <Text style={styles.textHeader}>Your profile</Text>
                            </View>
                            <View style={[styles.center, styles.headerBox2]}>
                                <Image source={require('./popcorn-icon.png')} style={styles.popcorn} />
                                <View>
                                    <TouchableOpacity onPress={this.goToHome}>
                                        <Text style={{ fontSize: 14, color: 'white', fontWeight: 'bold' }}>Back</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>

                        <View style={[styles.content]}>
                            <WhiteSpace></WhiteSpace>
                            <WhiteSpace></WhiteSpace>
                            <View style={styles.center}>
                                <TouchableOpacity onPress={() => this.selectImage()}>
                                    <Image source={{ uri: this.state.imagePath }} style={styles.profile} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.box}>
                                <Text style={styles.textStyle}>Email: {user[0].user.email}</Text>
                            </View>
                            <View style={styles.box}>
                                <Text style={styles.textStyle}>First name: {user[0].user.firstName}</Text>
                            </View>
                            <View style={styles.box}>
                                <Text style={styles.textStyle}>Last name: {user[0].user.lastName}</Text>
                            </View>

                        </View>
                    </View>

                    <View style={[styles.container2, styles.transparent, styles.border]} >
                        <View >
                            <Image source={require('./popcorn-icon.png')} style={styles.popcorn} />
                            <View style={styles.logoutButton}>
                                <TouchableOpacity onPress={this.goToLogin}>
                                    <Text style={styles.textButton}>Logout</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.logoutButton}>
                                <TouchableOpacity onPress={this.goToHistory}>
                                    <Text style={styles.textButton}>History</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                        {/* <View style={styles.box}>

                            <TouchableOpacity onPress={() => this.selectImage()}>
                                <Text >Register</Text>
                            </TouchableOpacity>

                        </View> */}
                        {/* <View style={styles.box}>
                            <Text style={styles.textStyle}>Last name: </Text>
                        </View> */}
                    </View>
                    <View style={styles.editButton}>
                        <TouchableOpacity onPress={this.gotoEditProfile}>
                            <Text style={styles.textButton}>Edit</Text>
                        </TouchableOpacity>
                    </View>

                </ImageBackground>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({

    contentContainer: {
        paddingVertical: 20
    },

    Background: {
        width: '100%',
        height: '100%',

    },

    container1: {
        flex: 1,
        flexDirection: 'column'
    },

    container2: {
        flex: 0.5,
        flexDirection: 'row'
    },

    header: {
        backgroundColor: '#AC3333',
        alignItems: 'center',
        flex: 0.15,
        flexDirection: 'row'
    },

    headerBox1: {
        flex: 1,
        backgroundColor: '#AC3333'
    },

    headerBox2: {
        flex: 0.3,
        backgroundColor: '#AC3333',
        flexDirection: 'row'
    },

    content: {
        flex: 1,
        flexDirection: 'column'

    },

    profile: {
        borderRadius: 40,
        width: 80,
        height: 80
    },

    box: {
        flex: 1,
        margin: 10,
        backgroundColor: 'white'
    },

    textStyle: {
        color: 'black',
        fontSize: 10,
        fontWeight: 'bold',
        padding: 15
    },

    popcorn: {
        width: 35,
        height: 35
    },

    usernameIcon: {
        borderRadius: 10,
        width: 20,
        height: 20
    },

    passwordIcon: {
        borderRadius: 10,
        width: 30,
        height: 30
    },

    editButton: {
        backgroundColor: '#AC3333',
        flex: 1,
        margin: 20,
        borderRadius: 5,
        width: 320,
        height: 40,
        alignItems: 'center'

    },

    logoutButton: {
        backgroundColor: '#AC3333',
        flex: 1,
        margin: 20,
        borderRadius: 5,
        width: 300,
        height: 40,
        alignItems: 'center'

    },

    textHeader: {
        color: 'white',
        fontSize: 14,
        fontWeight: 'bold',
        padding: 15
    },

    textStyle: {
        color: 'black',
        fontSize: 15,
        fontWeight: 'bold',
        padding: 15,
        textAlign: 'center'
    },

    textButton: {
        color: 'black',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 23
    },

    centerLayout1: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    centerLayout2: {
        alignItems: 'center',
        // justifyContent: 'center'
    },

    transparent: {
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
        // flex: 1,
        margin: 15
    },

    border: {
        borderRadius: 10
    },

    center: {
        alignItems: 'center',
    }

})
const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(Profile)

