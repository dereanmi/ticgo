import React, { Component } from 'react'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import Login from './Login/Login'
import Home from './Home/Home'
import Register from './Register/Register'
import Profile from './Profile/Profile'
// import History from './Profile/History'
import EditProfile from './Profile/EditProfile'
import MovieDetail from './MovieDetail/MovieDetail'
import Cinema from './Cinema/Cinema'
import { Provider } from 'react-redux';
import { store, history } from '../Reducers/AppStore'
import { ConnectedRouter } from 'connected-react-router'

class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                        <Switch>
                            <Route exact path="/login" component={Login} />
                            <Route exact path="/register" component={Register} />
                            <Route exact path="/home" component={Home} />
                            <Route exact path="/profile" component={Profile} />
                            <Route exact path="/editprofile" component={EditProfile} />
                            <Route exact path="/moviedetail" component={MovieDetail} />
                            <Route exact path="/cinema" component={Cinema} />
                            {/* <Route exact path="/history" component={History} /> */}
                            <Redirect to="/login" />
                        </Switch>
                </ConnectedRouter>
          </Provider>

                )
            }
        }

export default Router